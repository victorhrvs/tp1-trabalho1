/** file: test-ball.cpp
 ** brief: Tests the bouncing ball simulation
 ** author: Andrea Vedaldi
 **/

#include "ball.h"
#include <iostream>
#include <string>


using namespace std;

int main(int argc, char** argv)
{ /*

  ./test-ball <x0> <y0>
  Exemplo:
  ./test-ball 0.9 0.5

  */
  Ball ball ;
  if(argv[1]!=NULL && argv[2]!=NULL ){
  	double x0, y0;
  	x0 = std::stod(argv[1]);
  	y0 = std::stod(argv[2]);
  	cout << sizeof(int) << " " << x0 << " "<< y0 << endl;
  	ball.setPosicaoInicial(x0, y0);
  	}

  const double dt = 1.0/100 ;
  for (int i = 0 ; i < 10 ; ++i) {
    ball.step(dt);
    ball.display();
  }
  return 0 ;
}
