# Trabalho 1 de TP1


# Softwares utilizados
- **Make**     4.2.1-2
- **Freeglut** 3.0.0-1 Para o compilar a bibioteca openGl
- **g++** (GCC) 7.2.0 - Arch Linux (rolling-release 2017-10) - Compilador usado no desenvolvimento do projeto.

# Compilação e Execução
Para Compilar o ```test_ball```. Basta entrar no diretório e compilar usando o Make:
```
$ cd tp1-trabalho1/
$ make test-ball
```
Execução:
```
$ ./test-ball
```
Também é possível adicionar um valor inicial para a bola, adicionando o parametro ```-xyinit``` (Os valores de X e Y devem está entre 1 e -1):
```
$ ./test-ball 0.6 0.8
```


# Posição nos eixos X e Y:
[![N|Solid](https://i.imgur.com/SI4S0m5.png)]()
